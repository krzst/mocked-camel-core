package com.mockedcamel.core;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MockedCamelBoot {

    public static void main(String[] args) {
        SpringApplication.run(MockedCamelBoot.class, args);
    }

}