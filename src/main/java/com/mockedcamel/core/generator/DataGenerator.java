package com.mockedcamel.core.generator;

import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;
import com.mockedcamel.core.service.ModelService;

public abstract class DataGenerator<T extends Generatable> {

    public static final String SUFFIX = "Generator";

    protected abstract ModelService<T> getService();

    public T generate() {
        return getService().get();
    }

    public T generate(CreateMockRequest createMockRequest) {
        return getService().get(createMockRequest);
    }

}
