package com.mockedcamel.core;

import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Locale;
import java.util.Map;

@Configuration
@ConfigurationProperties
public class MockedCamelConfig {

    @Bean
    public Faker fakerBean(@Value("${mock.defaultLocale}") String fakeDefaultLocale) {
        return Faker.instance(Locale.forLanguageTag(fakeDefaultLocale));
    }

    public static void main(String[] args) throws Exception {
        Faker faker = Faker.instance(Locale.forLanguageTag("en"));

        FakeValuesService fakeValuesService = (FakeValuesService) FieldUtils.readField(faker, "fakeValuesService", true);
        List<Map<String, Object>> fakeValuesMaps = (List<Map<String, Object>>) FieldUtils.readField(fakeValuesService, "fakeValuesMaps", true);

        Gson gson = new Gson();
        JsonElement jsonElement = gson.toJsonTree(fakeValuesMaps);

        JsonArray jsonArray = jsonElement.getAsJsonArray();

        for (int i = 0; i < jsonArray.size(); i++) {
            if (jsonArray.get(i).getAsJsonObject().get("name") != null) {
                System.out.println(jsonArray.get(i).getAsJsonObject().get("name"));
            }
        }
    }



}
