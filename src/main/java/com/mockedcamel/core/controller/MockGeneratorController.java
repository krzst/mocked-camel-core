package com.mockedcamel.core.controller;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mockedcamel.core.generator.DataGenerator;
import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.GeneratedMockValue;
import com.mockedcamel.core.model.date.DateGenerator;
import com.mockedcamel.core.model.firstname.FirstNameGenerator;
import com.mockedcamel.core.model.lastname.LastNameGenerator;
import com.mockedcamel.core.model.number.NumberGenerator;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;
import org.reflections.Reflections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
public class MockGeneratorController {

    private ApplicationContext applicationContext;

    @Autowired
    public MockGeneratorController(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    @PostMapping(path = "/generate", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody Generatable generate(@RequestBody CreateMockRequest createMockRequest) {
        String mockType = createMockRequest.getType();

        Object mockGenerator = applicationContext.getBean(mockType + DataGenerator.SUFFIX);

        if (!(mockGenerator instanceof DataGenerator)) {
            throw new IllegalArgumentException("No generator for " + mockType);
        }

        DataGenerator dataGenerator = (DataGenerator) mockGenerator;
        Generatable generatable = dataGenerator.generate(createMockRequest);

        return  generatable;
    }

    @RequestMapping(path = {"/meta"})
    public Set<String> meta() {
        Reflections r = new Reflections("com.mockedcamel");

        Set<Class<?>> mocked = r.getTypesAnnotatedWith(GeneratedMockValue.class);
        Set<String> set = new HashSet<>();

        mocked.stream().forEach(m -> {
            set.add(m.getAnnotation(GeneratedMockValue.class).name());
        });

        return set;

    }

    @PostMapping(path = {"/example"}, consumes = MediaType.APPLICATION_JSON_UTF8_VALUE,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public String example(@RequestBody Map<String, Object> payload) {
        System.out.println(payload);

        List<Map> generators = (List<Map>) payload.get("x-schema-generators");
        Map<String, DataGenerator> dataGenerators = new HashMap<>();
        Map<String, CreateMockRequest> createMockRequests = new HashMap<>();
        generators.stream().forEach(generator -> {
            String alias = (String) generator.get("alias");
            String type = (String) generator.get("type");
            String subType = (String) generator.get("subType");
            Number min = (Number) generator.get("min");
            Number max = (Number) generator.get("max");

            System.out.println(alias + " -> " + type + " " + subType + " <" + min + "," + max + ">");

            Object mockGenerator = applicationContext.getBean(type + DataGenerator.SUFFIX);
            if (mockGenerator instanceof DataGenerator) {
                if (dataGenerators.containsKey(alias)) {
                    throw new IllegalArgumentException("Alias already defined for another generator");
                }

                dataGenerators.put(alias, (DataGenerator) mockGenerator);
                createMockRequests.put(alias, new CreateMockRequest().withType(type).withSubType(subType).withMin(min).withMax(max));
            }
        });


        FirstNameGenerator firstNameGenerator = applicationContext.getBean(FirstNameGenerator.class);
        LastNameGenerator lastNameGenerator = applicationContext.getBean(LastNameGenerator.class);
        NumberGenerator numberGenerator = applicationContext.getBean(NumberGenerator.class);
        DateGenerator dateGenerator = applicationContext.getBean(DateGenerator.class);

        JsonArray jsonArray = new JsonArray();
        JsonObject userJsonElement;
        for (int i = 0; i < 30; i++) {
            userJsonElement = new JsonObject();
            userJsonElement.addProperty("firstname", firstNameGenerator.generate().getValue());
            userJsonElement.addProperty("lastname", lastNameGenerator.generate().getValue());
            userJsonElement.addProperty("created", dateGenerator.generate(new CreateMockRequest().withMinDate("2000-01-01")).getValue().toString());
            userJsonElement.addProperty("name", numberGenerator.generate(new CreateMockRequest().withType("number").withSubType("integer").withMin(18).withMax(120)).getValue());

            jsonArray.add(userJsonElement);
        }

        return jsonArray.toString();

    }

}