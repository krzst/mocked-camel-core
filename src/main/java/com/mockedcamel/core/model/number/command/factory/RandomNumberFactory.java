package com.mockedcamel.core.model.number.command.factory;

import com.mockedcamel.core.model.number.command.*;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class RandomNumberFactory {

    public NumberCommand build(final CreateMockRequest createMockRequest) {
        Assert.notNull(createMockRequest, "Number request is not set");

        NumberCommand numberCommand = null;
        String numberType = createMockRequest.getFullType();

        if (numberType.equals(NumberCommand.Type.number_integer.name())) {
            if (createMockRequest.getMin() == null && createMockRequest.getMax() == null) {
                numberCommand = new NoRangeNumberCommand();
            } else {
                numberCommand = new RangeNumberCommand(createMockRequest);
            }
        }

        if (numberType.equals(NumberCommand.Type.number_float.name())) {
            if (createMockRequest.getMin() == null && createMockRequest.getMax() == null && createMockRequest.getScale() == null) {
                numberCommand = new NoRangeFloatCommand();
            }

            if (createMockRequest.getMin() != null && createMockRequest.getMax() != null && createMockRequest.getScale() != null) {
                numberCommand = new RangeScaleFloatCommand(createMockRequest);
            }
        }

        Assert.notNull(numberCommand, "Could not find a command to build random number");

        return numberCommand;
    }
}
