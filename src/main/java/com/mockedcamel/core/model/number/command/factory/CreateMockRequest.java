package com.mockedcamel.core.model.number.command.factory;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

public class CreateMockRequest {

    private String type;
    private String subType;

    @Valid
    @Min(value = Long.MIN_VALUE)
    private Number min;

    @Valid
    @Max(value = Long.MAX_VALUE)
    private Number max;

    @Valid
    @Positive
    private Number scale;

    // YYYY-MM-DD
    private String minDate;

    // YYYY-MM-DD
    private String maxDate;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSubType() {
        return subType;
    }

    public void setSubType(String subType) {
        this.subType = subType;
    }

    public Number getMin() {
        return min;
    }

    public void setMin(Number min) {
        this.min = min;
    }

    public Number getMax() {
        return max;
    }

    public void setMax(Number max) {
        this.max = max;
    }

    public Number getScale() {
        return scale;
    }

    public void setScale(Number scale) {
        this.scale = scale;
    }

    public String getMinDate() {
        return minDate;
    }

    public void setMinDate(String minDate) {
        this.minDate = minDate;
    }

    public String getMaxDate() {
        return maxDate;
    }

    public void setMaxDate(String maxDate) {
        this.maxDate = maxDate;
    }

    public final String getFullType() {
        String fullType = getType();

        if (getSubType() != null) {
            fullType += "_" + getSubType();
        }

        return fullType;
    }

    public CreateMockRequest withType(String type) {
        this.setType(type);

        return this;
    }

    public CreateMockRequest withSubType(String subType) {
        this.setSubType(subType);

        return this;
    }

    public CreateMockRequest withMin(Number min) {
        this.setMin(min);

        return this;
    }

    public CreateMockRequest withMax(Number max) {
        this.setMax(max);

        return this;
    }

    public CreateMockRequest withMinDate(String minDate) {
        setMinDate(minDate);

        return this;
    }

    public CreateMockRequest withMaxDate(String maxDate) {
        setMaxDate(maxDate);

        return this;
    }
}
