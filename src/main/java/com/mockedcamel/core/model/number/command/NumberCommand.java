package com.mockedcamel.core.model.number.command;

import com.mockedcamel.core.model.number.NumberMock;

public interface NumberCommand {

    enum Type {
        number_integer, number_float
    }

    NumberMock execute();
}
