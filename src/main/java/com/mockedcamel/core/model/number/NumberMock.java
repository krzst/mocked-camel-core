package com.mockedcamel.core.model.number;

import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.GeneratedMockValue;
import com.mockedcamel.core.model.generator.UniueKey;
import org.springframework.util.Assert;

@GeneratedMockValue(name = NumberMock.NAME)
public class NumberMock implements Generatable {

    public static final String NAME = "number";

    private Number value;
    private String key;

    public NumberMock(Number value) {
        Assert.notNull(value, "Value is null");

        this.value = value;
        this.key = UniueKey.uid();
    }

    @Override
    public String getKey() {
        return key;
    }

    public Number getValue() {
        return value;
    }
}
