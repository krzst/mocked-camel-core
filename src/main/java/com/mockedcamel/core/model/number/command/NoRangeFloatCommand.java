package com.mockedcamel.core.model.number.command;

import com.mockedcamel.core.model.number.NumberMock;
import io.github.benas.randombeans.randomizers.range.BigDecimalRangeRandomizer;

import java.math.BigDecimal;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class NoRangeFloatCommand implements NumberCommand {

    @Override
    public NumberMock execute() {
        long beforeDot = ThreadLocalRandom.current().nextLong();
        long afterDot = ThreadLocalRandom.current().nextLong(1, Long.MAX_VALUE);

        String randomValueString = beforeDot + "." + afterDot;
        BigDecimal randomValue = new BigDecimal(randomValueString.toCharArray());

        return new NumberMock(randomValue);
    }
}
