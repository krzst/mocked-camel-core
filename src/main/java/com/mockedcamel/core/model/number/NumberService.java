package com.mockedcamel.core.model.number;

import com.mockedcamel.core.model.number.command.NumberCommand;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;
import com.mockedcamel.core.model.number.command.factory.RandomNumberFactory;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class NumberService implements ModelService<NumberMock> {

    private RandomNumberFactory randomNumberFactory;

    @Autowired
    public NumberService(RandomNumberFactory randomNumberFactory) {
        this.randomNumberFactory = randomNumberFactory;
    }

    @Override
    public NumberMock get(CreateMockRequest createMockRequest) {
        NumberCommand numberCommand = randomNumberFactory.build(createMockRequest);

        return numberCommand.execute();
    }
}
