package com.mockedcamel.core.model.number.command;

import com.mockedcamel.core.model.number.NumberMock;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;

import java.util.concurrent.ThreadLocalRandom;

public class RangeNumberCommand implements NumberCommand {

    private CreateMockRequest createMockRequest;

    public RangeNumberCommand(CreateMockRequest createMockRequest) {
        if (createMockRequest.getMin().longValue() < Long.MIN_VALUE || createMockRequest.getMax().longValue()> Long.MAX_VALUE) {
            throw new IllegalArgumentException("Min and max not in the range <" + Long.MIN_VALUE + "," + Long.MAX_VALUE + ">");
        }

        this.createMockRequest = createMockRequest;
    }

    @Override
    public NumberMock execute() {
        Long randomNumber = ThreadLocalRandom.current().nextLong(createMockRequest.getMin().longValue(), createMockRequest.getMax().longValue());

        return new NumberMock(randomNumber);
    }
}
