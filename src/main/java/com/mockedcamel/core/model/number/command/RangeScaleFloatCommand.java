package com.mockedcamel.core.model.number.command;

import com.mockedcamel.core.model.number.NumberMock;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;

import java.math.BigDecimal;
import java.util.concurrent.ThreadLocalRandom;

public class RangeScaleFloatCommand implements NumberCommand {

    private CreateMockRequest createMockRequest;

    public RangeScaleFloatCommand(CreateMockRequest createMockRequest) {
        if (createMockRequest.getMin().longValue() < Long.MIN_VALUE || createMockRequest.getMax().longValue()> Long.MAX_VALUE
            || createMockRequest.getScale().longValue() <= 1L || createMockRequest.getScale().longValue() > 20) {
            throw new IllegalArgumentException("Min and max not in the range <" + Long.MIN_VALUE + "," + Long.MAX_VALUE + "> or scale is in a range (1,20)");
        }

        this.createMockRequest = createMockRequest;
    }

    @Override
    public NumberMock execute() {
        long min = createMockRequest.getMin().longValue();
        long max = createMockRequest.getMax().longValue();

        boolean hasScale = createMockRequest.getScale() != null;
        if (hasScale) {
            min++;
        }

        long beforeDot = ThreadLocalRandom.current().nextLong(min, max);

        long afterDot;
        if (!hasScale) {
            afterDot = ThreadLocalRandom.current().nextLong(1, Long.MAX_VALUE);
        } else {
            long scale = createMockRequest.getScale().longValue();

            long minAfterDot = 1;
            long maxAfterDot = 9;

            while (scale > 1) {
                maxAfterDot = maxAfterDot * 10 + 9;
                scale--;
            }

            afterDot = ThreadLocalRandom.current().nextLong(minAfterDot, maxAfterDot);
        }

        String randomValueString = beforeDot + "." + afterDot;
        BigDecimal randomValue = new BigDecimal(randomValueString.toCharArray());

        return new NumberMock(randomValue);
    }
}
