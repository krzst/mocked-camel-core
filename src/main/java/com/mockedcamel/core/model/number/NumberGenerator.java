package com.mockedcamel.core.model.number;

import com.mockedcamel.core.generator.DataGenerator;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = NumberMock.NAME + DataGenerator.SUFFIX)
public class NumberGenerator extends DataGenerator<NumberMock> {

    private NumberService numberService;

    @Autowired
    public NumberGenerator(NumberService numberService) {
        this.numberService = numberService;
    }

    @Override
    protected ModelService<NumberMock> getService() {
        return numberService;
    }
}
