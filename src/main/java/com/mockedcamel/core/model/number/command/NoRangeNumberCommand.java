package com.mockedcamel.core.model.number.command;

import com.mockedcamel.core.model.number.NumberMock;

import java.util.concurrent.ThreadLocalRandom;

public class NoRangeNumberCommand implements NumberCommand {

    @Override
    public NumberMock execute() {
        Long randomValue = ThreadLocalRandom.current().nextLong();

        return new NumberMock(randomValue);
    }
}
