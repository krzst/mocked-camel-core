package com.mockedcamel.core.model.date;

import com.mockedcamel.core.generator.DataGenerator;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = DateMock.NAME + DataGenerator.SUFFIX )
public class DateGenerator extends DataGenerator<DateMock> {

    private DateService dateService;

    @Autowired
    public DateGenerator(DateService dateService) {
        this.dateService = dateService;
    }

    @Override
    protected ModelService<DateMock> getService() {
        return dateService;
    }
}
