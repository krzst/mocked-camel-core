package com.mockedcamel.core.model.date;

import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;
import com.mockedcamel.core.service.ModelService;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.concurrent.ThreadLocalRandom;

@Component
public class DateService implements ModelService<DateMock> {

    @Override
    public DateMock get() {
        return get(new CreateMockRequest());
    }

    @Override
    public DateMock get(CreateMockRequest createMockRequest) {
        LocalDate now = LocalDate.now();
        LocalDate defaultMinLocalDate = now.minusYears(100); //start date

        String minDate = createMockRequest.getMinDate();
        String maxDate = createMockRequest.getMaxDate();

        LocalDate minLocalDate = getRangeLocalDate(minDate, defaultMinLocalDate);
        long start = minLocalDate.toEpochDay();

        LocalDate endDate = getRangeLocalDate(maxDate, now);
        long end = endDate.toEpochDay();

        long randomEpochDay = ThreadLocalRandom.current().longs(start, end).findAny().getAsLong();

        return new DateMock(LocalDate.ofEpochDay(randomEpochDay));
    }

    private LocalDate getRangeLocalDate(String rangeDate, LocalDate defaultRange) {
        int year;
        int month;
        int day;

        LocalDate defaultRangeDate;
        if (rangeDate != null && rangeDate.matches(DateMock.RANGE_DATE_FORMAT)) {
            String[] rangeDateElements = rangeDate.split(DateMock.RANGE_SEPARATOR);

            year = Integer.valueOf(rangeDateElements[0]);
            month = Integer.valueOf(rangeDateElements[1]);
            day = Integer.valueOf(rangeDateElements[2]);

            defaultRangeDate = LocalDate.of(year, month, day);
        } else {
            defaultRangeDate = defaultRange;
        }

        return defaultRangeDate;
    }
}
