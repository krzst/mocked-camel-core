package com.mockedcamel.core.model.date;

import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.GeneratedMockValue;
import com.mockedcamel.core.model.generator.UniueKey;
import org.springframework.util.Assert;

import java.time.LocalDate;

@GeneratedMockValue(name = DateMock.NAME)
public class DateMock implements Generatable {

    public static final String NAME = "date";
    public static final String RANGE_SEPARATOR = "-";
    public static final String RANGE_DATE_FORMAT = "\\d{4}" + RANGE_SEPARATOR + "\\d{2}" + RANGE_SEPARATOR + "\\d{2}";
    public static final String DEFAULT_MIN = "1900-01-01";

    private LocalDate value;
    private String key;

    public DateMock(LocalDate localDate) {
        Assert.notNull(localDate, "Value is null");

        this.value = localDate;
        this.key = UniueKey.uid();
    }

    @Override
    public String getKey() {
        return key;
    }

    public LocalDate getValue() {
        return value;
    }
}


