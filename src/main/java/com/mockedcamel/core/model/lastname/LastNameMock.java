package com.mockedcamel.core.model.lastname;

import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.GeneratedMockValue;
import com.mockedcamel.core.model.generator.UniueKey;
import org.springframework.util.Assert;

@GeneratedMockValue(name = LastNameMock.NAME)
public class LastNameMock implements Generatable {

    public static final String NAME = "lastname";

    private String value;
    private String key;

    public LastNameMock(String value) {
        Assert.notNull(value, "Value is null");

        this.value = value;
        this.key = UniueKey.uid();
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }
}
