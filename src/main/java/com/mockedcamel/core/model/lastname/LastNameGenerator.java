package com.mockedcamel.core.model.lastname;


import com.mockedcamel.core.generator.DataGenerator;
import com.mockedcamel.core.model.firstname.FirstNameMock;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = LastNameMock.NAME + DataGenerator.SUFFIX)
public class LastNameGenerator extends DataGenerator<LastNameMock> {

    @Autowired
    ModelService<LastNameMock> lastNameService;

    @Override
    protected ModelService<LastNameMock> getService() {
        return lastNameService;
    }
}
