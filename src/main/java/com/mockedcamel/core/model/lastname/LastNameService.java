package com.mockedcamel.core.model.lastname;

import com.github.javafaker.Faker;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class LastNameService implements ModelService<LastNameMock> {

    private Faker fakerBean;

    @Autowired
    LastNameService(Faker fakerBean) {
        Assert.notNull(fakerBean, "Faker bean is empty");

        this.fakerBean = fakerBean;
    }

    @Override
    public LastNameMock get() {
        return new LastNameMock(fakerBean.name().lastName());
    }

    @Override
    public LastNameMock get(CreateMockRequest createMockRequest) {
        return get();
    }
}
