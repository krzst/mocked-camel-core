package com.mockedcamel.core.model;

import java.lang.annotation.*;

@Documented
@Target(ElementType.TYPE)
@Inherited
@Retention(RetentionPolicy.RUNTIME)
public @interface GeneratedMockValue {

    String name();

}
