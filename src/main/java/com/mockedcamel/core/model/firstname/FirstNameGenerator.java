package com.mockedcamel.core.model.firstname;


import com.mockedcamel.core.generator.DataGenerator;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component(value = FirstNameMock.NAME + DataGenerator.SUFFIX )
public class FirstNameGenerator extends DataGenerator<FirstNameMock> {

    @Autowired
    ModelService<FirstNameMock> firstNameService;

    @Override
    public ModelService<FirstNameMock> getService() {
        return firstNameService;
    }
}
