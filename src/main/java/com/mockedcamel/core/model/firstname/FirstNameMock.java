package com.mockedcamel.core.model.firstname;

import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.GeneratedMockValue;
import com.mockedcamel.core.model.generator.UniueKey;
import org.springframework.util.Assert;

@GeneratedMockValue(name = FirstNameMock.NAME)
public class FirstNameMock implements Generatable {

    public static final String NAME = "firstname";

    private String value;
    private String key;

    public FirstNameMock(String value) {
        Assert.notNull(value, "Value is null");

        this.value = value;
        this.key = UniueKey.uid();
    }

    public String getValue() {
        return value;
    }

    public String getKey() {
        return key;
    }
}
