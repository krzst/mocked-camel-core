package com.mockedcamel.core.model.firstname;

import com.github.javafaker.Faker;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;
import com.mockedcamel.core.service.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;

@Component
public class FirstNameService implements ModelService<FirstNameMock> {

    private Faker fakerBean;

    @Autowired
    FirstNameService(Faker fakerBean) {
        Assert.notNull(fakerBean, "Faker bean is empty");

        this.fakerBean = fakerBean;
    }

    @Override
    public FirstNameMock get() {
        return new FirstNameMock(fakerBean.name().firstName());
    }

    @Override
    public FirstNameMock get(CreateMockRequest createMockRequest) {
        return get();
    }
}
