package com.mockedcamel.core.model;

public interface Generatable {

    String getKey();

}
