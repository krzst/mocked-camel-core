package com.mockedcamel.core.service;

import com.mockedcamel.core.model.Generatable;
import com.mockedcamel.core.model.number.command.factory.CreateMockRequest;

public interface ModelService<T extends Generatable> {

    default T get() {
        throw new IllegalArgumentException("Not implemented");
    }

    default T get(CreateMockRequest createMockRequest) {
        throw new IllegalArgumentException("Not implemented");
    }

}
